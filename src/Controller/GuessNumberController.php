<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class GuessNumberController extends AbstractController
{

    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/guess-number", name="guess")
     */
    public function guessNumber(): Response
    {

        $number = 0;
        $message = "";

        if ($this->session->get('number') != null) {
            $number = $this->session->get('number');
        } else {
            $this->session->set('number', random_int(0, 100));
            $number = $this->session->get('number');
        }

        if (!empty($_GET["input_number"])) {
            if ($_GET["input_number"] > $number) {
                $message = "PREA MARE";
            } else if ($_GET["input_number"] < $number) {
                $message = "PREA MIC";
            } else {
                $message = "EXACT";
            }
        }

        return $this->render('guess_number/guess_number.html.twig', [
            'number' => $number,
            'message' => $message
        ]);
    }
}
